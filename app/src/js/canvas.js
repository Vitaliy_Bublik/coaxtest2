import perfectCity from './perfectCity'

export default class canva {


  // import perfectCity function
  getSum(departure, destination) { return perfectCity(departure, destination)}





  // settings
  constructor() {
    this.tw         = 600                                             // table width
    this.th         = 600                                             // table height
    this.tdx        = 10                                              // table col count
    this.tdy        = 10                                              // table row count
    this.r1         = 10                                              // first radius
    this.r2         = 10                                              // second radius
    this.rgb1       = '168, 236, 249'                                 // color of first point
    this.rgb2       = '168, 236, 249'                                 // color of second point
    this.lineW      = 2                                               // width of line
    this.lineRgba   = '100, 155, 255, .75'                            // color of line
    this.deformX    = .982                                            // deformation of draw zone by x
    this.deformY    = .982                                            // deformation of draw zone by y
    this.corectionX = 5                                               // correction by x
    this.corectionY = 5                                               // correction by y

    this.p          = document.querySelector('canvas').parentNode     // parent of canvas
    this.c          = document.querySelector('canvas')                // canvas
    this.ctx        = this.c.getContext('2d')
    this.start()
  }





  start() {
    this.getSize()
    this.onEvent()
  }





  // set canvas size
  getSize() {
    this.c.width  = this.c.parentNode.offsetWidth
    this.c.height = this.c.parentNode.offsetHeight
  }





  // on click
  onEvent() {
    this.p.addEventListener('click', ( event ) => {
      if( event.target.tagName !== 'TABLE' && event.target.tagName !== 'TBODY' ) return


      let pX = +((event.pageX - this.c.getBoundingClientRect().left) / this.c.width).toFixed(2)
      let pY = +((event.pageY - this.c.getBoundingClientRect().top) / this.c.height).toFixed(2)


      if( pX * 10 != Math.round(pX * 10) && pY * 10 != Math.round(pY * 10) ) {
        if( Math.abs(pX * 10 - Math.round(pX * 10)) < Math.abs(pY * 10 - Math.round(pY * 10)) )
          pX = +(Math.round(pX * 10) / 10).toFixed(2)
        else
          pY = +(Math.round(pY * 10) / 10).toFixed(2)
      }

      this.getCoords( pX, pY )
      this.drawPoint()
      if( this.mouseX1 != undefined && this.mouseX2 != undefined ) this.drawLines()
    })
  }





  // get coordinates
  getCoords( pX, pY ) {
    if (this.mouseX1 !== undefined && this.mouseX2 !== undefined || this.mouseX1 == undefined) {
      this.mouseX1 = this.mouseX2
      this.mouseY1 = this.mouseY2
      this.mouseX2 = Math.round(this.c.width  * pX)
      this.mouseY2 = Math.round(this.c.height * pY)
    } else {
      this.mouseX1 = Math.round(this.c.width  * pX)
      this.mouseY1 = Math.round(this.c.height * pY)
    }
  }





  // draw 2 points
  drawPoint() {
    let ctx = this.ctx

    let x1 = this.mouseX1 * this.deformX + this.corectionX
    let y1 = this.mouseY1 * this.deformY + this.corectionY

    let x2 = this.mouseX2 * this.deformX + this.corectionX
    let y2 = this.mouseY2 * this.deformY + this.corectionY

    if( this.mouseX1 != undefined && this.mouseX2 != undefined ) {
      let dep = [Math.round(this.mouseX1 / (this.tw * .01)) / 10, Math.round(this.mouseY1 / (this.th * .01)) / 10]
      let des = [Math.round(this.mouseX2 / (this.tw * .01)) / 10, Math.round(this.mouseY2 / (this.th * .01)) / 10]
      let res = this.getSum( dep, des )
      this.wrightResult( dep, des, res )
    }

    ctx.clearRect(0, 0, this.c.width, this.c.height)

    ctx.beginPath()
      let gradient2 = ctx.createRadialGradient(x2, y2, 0, x2, y2, this.r2)
      gradient2.addColorStop(.2, 'rgba(' + this.rgb2 + ', 1)')
      gradient2.addColorStop(.3, 'rgba(' + this.rgb2 + ', .3)')
      ctx.arc(x2, y2, this.r2, 0, 2 * Math.PI)
      ctx.fillStyle = gradient2
      ctx.fill()
    ctx.closePath()

    if( this.mouseX1 == undefined ) return

    ctx.beginPath()
      let gradient1 = ctx.createRadialGradient(x1, y1, 0, x1, y1, this.r1)
      gradient1.addColorStop(.2, 'rgba(' + this.rgb1 + ', 1)')
      gradient1.addColorStop(.3, 'rgba(' + this.rgb1 + ', .3)')
      ctx.arc(x1, y1, this.r1, 0, 2 * Math.PI)
      ctx.fillStyle = gradient1
      ctx.fill()
    ctx.closePath()
  }





  // raw lines
  drawLines() {
    let sx11 = 0
    let sy11 = 0
    let sx22 = 0
    let sy22 = 0

    let mX1 = this.mouseX1
    let mY1 = this.mouseY1
    let mX2 = this.mouseX2
    let mY2 = this.mouseY2

    let smallX1 = +(mX1 / (this.tw / this.tdx)).toFixed(1)
    let smallY1 = +(mY1 / (this.th / this.tdy)).toFixed(1)
    let smallX2 = +(mX2 / (this.tw / this.tdx)).toFixed(1)
    let smallY2 = +(mY2 / (this.th / this.tdy)).toFixed(1)

    if( smallX1 != Math.round(smallX1) && smallY1 != Math.round(smallY1) ) {
      if( Math.abs(smallX1 - Math.round(smallX1)) < Math.abs(smallY1 - Math.round(smallY1)) )
        smallX1 = +(Math.round(smallX1)).toFixed(1)
      else
        smallY1 = +(Math.round(smallY1)).toFixed(1)
    }

    if( smallX2 != Math.round(smallX2) && smallY2 != Math.round(smallY2) ) {
      if( Math.abs(smallX2 - Math.round(smallX2)) < Math.abs(smallY2 - Math.round(smallY2)) )
        smallX2 = +(Math.round(smallX2)).toFixed(1)
      else
        smallY2 = +(Math.round(smallY2)).toFixed(1)
    }

    let depX = Math.floor(smallX1)
    let depY = Math.floor(smallY1)
    let desX = Math.floor(smallX2)
    let desY = Math.floor(smallY2)


    if(
      depX - desX == 0 &&
      depY - desY == 0 ||
      depX - desX != 0 &&
      depY - desY != 0
    ) {

      if( smallX1 < smallX2 ) {

        if (smallY1 < smallY2) {
          if( smallX1 != depX && smallX2 != desX ){ sx11 = 1 - (smallX1 - depX) ; sx22 = -(smallX2 - smallX1) + sx11  }
          if( smallX1 != depX && smallX2 == desX ){ sx11 = smallX2 - smallX1    ; sy22 = -(smallY2 - smallY1)         }
          if( smallX1 == depX && smallX2 != desX ){ sy11 = smallY2 - smallY1                                          }
          if( smallX1 == depX && smallX2 == desX ){ sy11 = 1 - (smallY1 - depY) ; sy22 = -(smallY2 - smallY1) + sy11  }
        } else {
          if( smallX1 != depX && smallX2 != desX ){  sx11 = 1 - (smallX1 - depX); sx22 = -(smallX2 - smallX1) + sx11  }
          if( smallX1 != depX && smallX2 == desX ){  sx11 = smallX2 - smallX1   ; sy22 = smallY1 - smallY2            }
          if( smallX1 == depX && smallX2 != desX ){  sy11 = -(smallY1 - smallY2)                                      }
          if( smallX1 == depX && smallX2 == desX ){  sy11 = -(smallY1 - depY)   ; sy22 = -(smallY2 - smallY1) + sy11  }
        }
      } else {

        if (smallY1 < smallY2) {
          if( smallX1 != depX && smallX2 != desX ){  sx11 = -(smallX1 - depX)   ; sx22 = (smallX1 - smallX2) + sx11   }
          if( smallX1 != depX && smallX2 == desX ){  sx11 = -(smallX1 - smallX2); sy22 = -(smallY2 - smallY1)         }
          if( smallX1 == depX && smallX2 != desX ){  sy11 = smallY2 - smallY1                                         }
          if( smallX1 == depX && smallX2 == desX ){  sy11 = 1 - (smallY1 - depY); sy22 = -(smallY2 - smallY1) + sy11  }
        } else {
          if( smallX1 != depX && smallX2 != desX ){  sx11 = -(smallX1 - depX)   ; sx22 = (smallX1 - smallX2) + sx11   }
          if( smallX1 != depX && smallX2 == desX ){  sx11 = -(smallX1 - smallX2); sy22 = smallY1 - smallY2            }
          if( smallX1 == depX && smallX2 != desX ){  sy11 = -(smallY1 - smallY2)                                      }
          if( smallX1 == depX && smallX2 == desX ){  sy11 = -(smallY1 - depY)   ; sy22 = (smallY1 - smallY2) + sy11   }
        }
      }

    } else if(depX - desX == 0) {

      var sx1 = smallX1 - depX
      var sx2 = smallX2 - desX
      sx11 = sx1 + sx2 < 1 ? -sx1 : (1 - sx1)
      sx22 = sx1 + sx2 < 1 ? -sx2 : (1 - sx2)

    } else if(depY - desY == 0) {

      var sy1 = smallY1 - depY
      var sy2 = smallY2 - desY
      sy11 = sy1 + sy2 < 1 ? -sy1 : (1 - sy1)
      sy22 = sy1 + sy2 < 1 ? -sy2 : (1 - sy2)

    }

    if(
      depX - desX == 0 && smallY1 == smallY2 ||
      depY - desY == 0 && smallX1 == smallX2
    ) {
      sx11 = sy11 = sx22 = sy22 = 0
    }



    // draw line
    this.line( mX1, mY1, mX2, mY2, sx11, sy11, sx22, sy22 )
  }





  // line func
  line( mX1, mY1, mX2, mY2, sx11, sy11, sx22, sy22 ) {
    let ctx = this.ctx

    ctx.beginPath()
      ctx.moveTo( mX1 * this.deformX + this.corectionX, mY1 * this.deformY + this.corectionY)
      ctx.lineTo((mX1 + (sx11 || 0) * (this.tw / this.tdx)) * this.deformX + this.corectionX, (mY1 + (sy11 || 0) * (this.th / this.tdy)) * this.deformY + this.corectionY)
      ctx.lineTo((mX2 + (sx22 || 0) * (this.tw / this.tdx)) * this.deformX + this.corectionX, (mY2 + (sy22 || 0) * (this.th / this.tdy)) * this.deformY + this.corectionY)
      ctx.lineTo( mX2 * this.deformX + this.corectionX, mY2 * this.deformY + this.corectionX)

      ctx.lineWidth = this.lineW
      ctx.strokeStyle = 'rgba(' + this.lineRgba + ')'
      ctx.fillStyle = 'rgba(0,0,0,0)'
      ctx.lineCap = "round"
      ctx.stroke();
    ctx.closePath()
  }





  // wright result in top of canvas
  wrightResult( dep, des, res ) {
    if( dep !== undefined ) document.querySelector('.departure').innerHTML    = `[${dep}]`
    if( des !== undefined ) document.querySelector('.destination').innerHTML  = `[${des}]`
    if( res !== undefined ) document.querySelector('.sum').innerHTML          = res
  }
}