import canva from './canvas'

let canvas = new canva


export default function perfectCity(departure, destination) {

  let depX = Math.floor(departure[0])
  let depY = Math.floor(departure[1])
  let desX = Math.floor(destination[0])
  let desY = Math.floor(destination[1])

  if(
    depX - desX == 0 && depY - desY == 0 ||
    depX - desX != 0 && depY - desY != 0
  ) {

    var sx = Math.abs(departure[0] - destination[0])
    var sy = Math.abs(departure[1] - destination[1])

  } else if(depX - desX == 0) {

    var sx1 = departure[0] - depX
    var sx2 = destination[0] - desX
    var sx = sx1 + sx2 < 1 ? sx1 + sx2 : (1 - sx1) + (1 - sx2)
    var sy = Math.abs(departure[1] - destination[1])

  } else if(depY - desY == 0) {

    var sy1 = departure[1] - depY
    var sy2 = destination[1] - desY
    var sy = sy1 + sy2 < 1 ? sy1 + sy2 : (1 - sy1) + (1 - sy2)
    var sx = Math.abs(departure[0] - destination[0])

  }

  return +(sx + sy).toFixed(1)
}